import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.io.RandomAccessFile
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes
import java.util.TreeMap
import java.util.ArrayList
import java.util.Collections
import java.util.Comparator
import java.util.LinkedList

object Index {

    // Term id -> (position in index file, doc frequency) dictionary
    private val postingDict = TreeMap<Integer, Pair<Long, Integer>>()
    // Doc name -> doc id dictionary
    private val docDict = TreeMap<String, Integer>()
    // Term -> term id dictionary
    private val termDict = TreeMap<String, Integer>()
    // Block queue
    private val blockQueue = LinkedList<File>()

    // Total file counter
    private var totalFileCount = 0
    // Document counter
    private var docIdCounter = 0
    // Term counter
    private var wordIdCounter = 0
    // Index
    private var index: BaseIndex? = null

    private var sPoint: Long = 0
    /*
	 * Write a posting list to the given file
	 * You should record the file position of this posting list
	 * so that you can read it back during retrieval
	 *
	 * */
    @Throws(IOException::class)
    private fun writePosting(fc: FileChannel, posting: PostingList) {
        /*
		 * TODO: Your code here
		 *
		 */
        val frequency = posting.getList().size()
        val termId = posting.getTermId()
        //System.out.println("Before postingdict"+postingDict);
        index!!.writePosting(fc, posting)
        val order = Pair<Long, Integer>(sPoint, frequency)
        postingDict.put(termId, order)
        //System.out.println(frequency);
        //System.out.println(fc.size());
        sPoint += (8 + frequency * 4).toLong() // check between fc.position;
        //System.out.println("After postingDict"+postingDict);
        //System.out.println("posting "+sPoint);

    }


    /**
     * Pop next element if there is one, otherwise return null
     * @param iter an iterator that contains integers
     * @return next element or null
     */
    private fun popNextOrNull(iter: Iterator<Integer>): Integer? {
        return if (iter.hasNext()) {
            iter.next()
        } else {
            null
        }
    }


    /**
     * Main method to start the indexing process.
     * @param method        :Indexing method. "Basic" by default, but extra credit will be given for those
     * who can implement variable byte (VB) or Gamma index compression algorithm
     * @param dataDirname    :relative path to the dataset root directory. E.g. "./datasets/small"
     * @param outputDirname    :relative path to the output directory to store index. You must not assume
     * that this directory exist. If it does, you must clear out the content before indexing.
     */
    @Throws(IOException::class)
    fun runIndexer(method: String, dataDirname: String, outputDirname: String): Int {
        /* Get index */
        val className = method + "Index"
        try {
            val indexClass = Class.forName(className)
            index = indexClass.newInstance() as BaseIndex
        } catch (e: Exception) {
            System.err
                    .println("Index method must be \"Basic\", \"VB\", or \"Gamma\"")
            throw RuntimeException(e)
        }

        /* Get root directory */
        val rootdir = File(dataDirname)
        if (!rootdir.exists() || !rootdir.isDirectory()) {
            System.err.println("Invalid data directory: $dataDirname")
            return -1
        }


        /* Get output directory*/
        val outdir = File(outputDirname)
        if (outdir.exists() && !outdir.isDirectory()) {
            System.err.println("Invalid output directory: $outputDirname")
            return -1
        }

        /*	TODO: delete all the files/sub folder under outdir
		 *
		 */

        delete(outdir)
        if (!outdir.exists()) {
            if (!outdir.mkdirs()) {
                System.err.println("Create output directory failure")
                return -1
            }
        }


        /* BSBI indexing algorithm */
        val dirlist = rootdir.listFiles()

        wordIdCounter = 0

        /* For each block */
        for (block in dirlist) {
            val posting = TreeMap<Integer, PostingList>()//id+docid
            val blockFile = File(outputDirname, block.getName())
            System.out.println("Processing block " + block.getName())
            blockQueue.add(blockFile)

            val blockDir = File(dataDirname, block.getName())
            val filelist = blockDir.listFiles()

            /* For each file */
            for (file in filelist) {
                ++totalFileCount
                val fileName = block.getName() + "/" + file.getName()

                // use pre-increment to ensure docID > 0
                val docId = ++docIdCounter
                docDict.put(fileName, docId)


                val reader = BufferedReader(FileReader(file))
                var line: String
                while ((line = reader.readLine()) != null) {
                    val tokens = line.trim().split("\\s+")
                    for (token in tokens) {
                        /*
						 * TODO: Your code here
						 *       For each term, build up a list of
						 *       documents in which the term occurs
						 */
                        if (!termDict.containsKey(token)) {
                            wordIdCounter++
                            termDict.put(token, wordIdCounter)
                        }
                        if (posting.containsKey(termDict.get(token))) {
                            if (!posting.get(termDict.get(token)).getList().contains(docId)) {  //checkkkk  no ! will be add same word on it
                                posting.get(termDict.get(token)).getList().add(docId)
                            }
                        } else if (!posting.containsKey(termDict.get(token))) {
                            val newPost = PostingList(termDict.get(token))
                            newPost.getList().add(docId)
                            posting.put(termDict.get(token), newPost)
                        }


                    }
                }
                reader.close()
            }

            /* Sort and output */
            if (!blockFile.createNewFile()) {
                System.err.println("Create new block failure.")
                return -1
            }

            val bfc = RandomAccessFile(blockFile, "rw")

            /*
			 * TODO: Your code here
			 *       Write all posting lists for all terms to file (bfc)
			 */
            val fileCh = bfc.getChannel()
            var specialcheck = 0
            for (i in posting.keySet()) {
                //				System.out.println("check");
                specialcheck = 1
                writePosting(fileCh, posting.get(i))
            }
            if (specialcheck == 1) {
                System.out.println("check")
            }
            bfc.close()
        }

        /* Required: output total number of files. */
        //System.out.println("Total Files Indexed: "+totalFileCount);

        /* Merge blocks */
        while (true) {
            if (blockQueue.size() <= 1)
                break

            val b1 = blockQueue.removeFirst()
            val b2 = blockQueue.removeFirst()

            val combfile = File(outputDirname, b1.getName() + "+" + b2.getName())
            if (!combfile.createNewFile()) {
                System.err.println("Create new block failure.")
                return -1
            }

            val bf1 = RandomAccessFile(b1, "r")
            val bf2 = RandomAccessFile(b2, "r")
            val mf = RandomAccessFile(combfile, "rw")

            /*
			 * TODO: Your code here
			 *       Combine blocks bf1 and bf2 into our combined file, mf
			 *       You will want to consider in what order to merge
			 *       the two blocks (based on term ID, perhaps?).
			 *
			 */
            //bytebuffer

            val checktest = 0
            //setposition
            var position: Long = 0 //check to use file.getposition
            var posRead = index!!.readPosting(bf1.getChannel().position(position))
            val postingMerge = TreeMap<Integer, PostingList>()//id+docid
            //System.out.println(posRead);
            while (posRead != null) {
                //				if(checktest==0){
                //					posRead = index.readPosting(bf1.getChannel().position(position));
                //					checktest++;
                //				}
                postingMerge.put(posRead!!.getTermId(), posRead)// input data to treeMap from 1 then add 2 to it
                //position+=8+bf1.length();
                //System.out.println("position ="+position );
                position += 8 + 4 * posRead!!.getList().size()
                //System.out.println("position ="+position );
                posRead = index!!.readPosting(bf1.getChannel().position(position))
            }

            //System.out.println("Position check "+position);
            position = 0
            //int positionbf2=0;
            posRead = index!!.readPosting(bf2.getChannel().position(position))
            while (posRead != null) {
                if (postingMerge.containsKey(posRead!!.getTermId())) {
                    for (i in posRead!!.getList()) {
                        if (!postingMerge.get(posRead!!.getTermId()).getList().contains(i)) {
                            postingMerge.get(posRead!!.getTermId()).getList().add(i)
                        }
                    }

                } else {
                    postingMerge.put(posRead!!.getTermId(), posRead)
                }
                //				if(postingMerge.containsKey(posRead.getTermId())){
                //					for(int i: posRead.getList()){
                //						if(!postingMerge.get(posRead.getTermId()).getList().contains(i)){
                //							postingMerge.get(posRead.getTermId()).getList().add(i);
                //						}
                //					}
                //				}
                //				else{
                //					postingMerge.put(posRead.getTermId(),posRead);
                //				}
                position += 8 + 4 * posRead!!.getList().size()
                posRead = index!!.readPosting(bf2.getChannel().position(position))

            }
            sPoint = 0
            for (i in postingMerge.keySet()) {
                Collections.sort(postingMerge.get(i).getList())
                writePosting(mf.getChannel(), postingMerge.get(i))
            }




            bf1.close()
            bf2.close()
            mf.close()
            b1.delete()
            b2.delete()
            blockQueue.add(combfile)
        }

        /* Dump constructed index back into file system */
        val indexFile = blockQueue.removeFirst()
        indexFile.renameTo(File(outputDirname, "corpus.index"))

        val termWriter = BufferedWriter(FileWriter(File(
                outputDirname, "term.dict")))
        for (term in termDict.keySet()) {
            termWriter.write(term + "\t" + termDict.get(term) + "\n")
        }
        termWriter.close()

        val docWriter = BufferedWriter(FileWriter(File(
                outputDirname, "doc.dict")))
        for (doc in docDict.keySet()) {
            docWriter.write(doc + "\t" + docDict.get(doc) + "\n")
        }
        docWriter.close()

        val postWriter = BufferedWriter(FileWriter(File(
                outputDirname, "posting.dict")))
        for (termId in postingDict.keySet()) {
            postWriter.write(termId + "\t" + postingDict.get(termId).getFirst()
                    + "\t" + postingDict.get(termId).getSecond() + "\n")
        }
        postWriter.close()

        return totalFileCount
    }

    private fun delete(deleteFile: File) {
        // TODO Auto-generated method stub
        if (deleteFile.isFile()) {
            for (fi in deleteFile.listFiles()) {
                fi.delete()
                //delete(fi);
            }
        }
    }


    @Throws(IOException::class)
    fun main(args: Array<String>) {
        /* Parse command line */
        if (args.size != 3) {
            System.err
                    .println("Usage: java Index [Basic|VB|Gamma] data_dir output_dir")
            return
        }

        /* Get index */
        var className = ""
        try {
            className = args[0]
        } catch (e: Exception) {
            System.err
                    .println("Index method must be \"Basic\", \"VB\", or \"Gamma\"")
            throw RuntimeException(e)
        }

        /* Get root directory */
        val root = args[1]


        /* Get output directory */
        val output = args[2]
        runIndexer(className, root, output)
    }


}

