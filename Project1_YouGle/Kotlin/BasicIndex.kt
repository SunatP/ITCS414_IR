import java.awt.List
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.util.ArrayList


class BasicIndex : BaseIndex {
    @Override
    fun readPosting(fc: FileChannel): PostingList? {
        /*
		 * TODO: Your code here
		 *       Read and return the postings list from the given file.
		 */
        val WID: Int
        val frequencycheck: Int
        val bfunction = ByteBuffer.allocate(4)
        var simulatePosting: PostingList? = null
        try {
            if (fc.size() > fc.position()) {
                var RBy = fc.read(bfunction)
                bfunction.flip() // cant remove dont forget
                WID = bfunction.getInt()
                bfunction.clear()
                simulatePosting = PostingList(WID)
                RBy = fc.read(bfunction)
                bfunction.flip()
                frequencycheck = bfunction.getInt()
                bfunction.clear()
                for (i in 0 until frequencycheck) {
                    RBy = fc.read(bfunction)
                    bfunction.flip()
                    val docId = bfunction.getInt()
                    simulatePosting!!.getList().add(docId)
                    bfunction.clear()
                }
            } else {
                return null
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return simulatePosting
    }

    @Override
    fun writePosting(fc: FileChannel, p: PostingList) {
        /*
		 * TODO: Your code here
		 *       Write the given postings list to the given file.
		 */
        //		ByteBuffer bfunction = ByteBuffer.allocate(4+4+(4*p.getList().size()));
        //		bfunction.putInt(p.getTermId());
        //		bfunction.putInt(p.getList().size());
        //		for(int docId: p.getList()){
        //			bfunction.putInt(docId);
        //		}
        //
        //			bfunction.flip();
        //			fc.write(bfunction);
        //			bfunction.clear();
        val check = ArrayList<Integer>()
        val num = ArrayList<Integer>()
        val bfunction = ByteBuffer.allocate(1048576)
        //		check.add(p.getTermId());
        //		check.add(p.getList().size());
        //		for(int i=0;i<100;i++){
        //			System.out.println("check:"+check.get(i));
        //		}
        num.add(p.getTermId())
        num.add(p.getList().size())
        //		for(int i=0;i<num.size();i++){
        //			System.out.println(num.get(i));
        //		}
        for (i in 0 until p.getList().size()) {
            num.add(p.getList().get(i))
        }
        val j = num.size()
        var gS = arrayOfNulls<Integer>(j)
        gS = num.toArray(gS)
        //System.out.println("Check size = " + getSize);
        for (i in gS.indices) {
            bfunction.putInt(gS[i])
        }
        bfunction.flip() // cant remove wtf
        try {
            fc.write(bfunction)
            //System.out.println(bfunction);
            //System.out.println("check basicindex");
            bfunction.clear()

        } catch (e: IOException) {
            e.printStackTrace()
            System.out.println("BASICINDEX LINE 92")
        }


    }
}

