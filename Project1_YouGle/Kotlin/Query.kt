import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.RandomAccessFile
import java.nio.channels.FileChannel
import java.util.ArrayList
import java.util.Collections
import java.util.Comparator
import java.util.TreeMap

class Query {

    // Term id -> position in index file
    private val posDict = TreeMap<Integer, Long>()
    // Term id -> document frequency
    private val freqDict = TreeMap<Integer, Integer>()
    // Doc id -> doc name dictionary
    private val docDict = TreeMap<Integer, String>()
    // Term -> term id dictionary
    private val termDict = TreeMap<String, Integer>()
    // Index
    private var index: BaseIndex? = null


    //indicate whether the query service is running or not
    private var running = false
    private var indexFile: RandomAccessFile? = null

    /*
	 * Read a posting list with a given termID from the file
	 * You should seek to the file position of this specific
	 * posting list and read it back.
	 * */
    @Throws(IOException::class)
    private fun readPosting(fc: FileChannel, termId: Int): PostingList {
        /*
		 * TODO: Your code here
		 */
        fc.position(posDict.get(termId))
        return index!!.readPosting(fc)
    }


    @Throws(IOException::class)
    fun runQueryService(indexMode: String?, indexDirname: String) {
        //Get the index reader
        try {
            val indexClass = Class.forName(indexMode!! + "Index")
            index = indexClass.newInstance() as BaseIndex
        } catch (e: Exception) {
            System.err
                    .println("Index method must be \"Basic\", \"VB\", or \"Gamma\"")
            throw RuntimeException(e)
        }

        //Get Index file
        val inputdir = File(indexDirname)
        if (!inputdir.exists() || !inputdir.isDirectory()) {
            System.err.println("Invalid index directory: $indexDirname")
            return
        }

        /* Index file */
        indexFile = RandomAccessFile(File(indexDirname,
                "corpus.index"), "r")

        var line: String? = null
        /* Term dictionary */
        val termReader = BufferedReader(FileReader(File(
                indexDirname, "term.dict")))
        while ((line = termReader.readLine()) != null) {
            val tokens = line!!.split("\t")
            termDict.put(tokens[0], Integer.parseInt(tokens[1]))
        }
        termReader.close()

        /* Doc dictionary */
        val docReader = BufferedReader(FileReader(File(
                indexDirname, "doc.dict")))
        while ((line = docReader.readLine()) != null) {
            val tokens = line!!.split("\t")
            docDict.put(Integer.parseInt(tokens[1]), tokens[0])
        }
        docReader.close()

        /* Posting dictionary */
        val postReader = BufferedReader(FileReader(File(
                indexDirname, "posting.dict")))
        while ((line = postReader.readLine()) != null) {
            val tokens = line!!.split("\t")
            posDict.put(Integer.parseInt(tokens[0]), Long.parseLong(tokens[1]))
            freqDict.put(Integer.parseInt(tokens[0]),
                    Integer.parseInt(tokens[2]))
        }
        postReader.close()

        this.running = true
    }

    @Throws(IOException::class)
    fun retrieve(query: String): List<Integer>? {
        if (!running) {
            System.err.println("Error: Query service must be initiated")
        }

        /*
		 * TODO: Your code here
		 *       Perform query processing with the inverted index.
		 *       return the list of IDs of the documents that match the query
		 *
		 */
        val tokens = query.split("\\s+")
        val list = ArrayList<List<Integer>>()
        for (token in tokens) {
            if (termDict.containsKey(token)) {
                list.add(readPosting(indexFile!!.getChannel(), termDict.get(token)).getList())
            }
        }
        val iter = list.iterator()
        var l1: List<Integer>? = popNextOrNull(iter)
        var l2: List<Integer>?

        while (l1 != null && (l2 = popNextOrNull(iter)) != null)
            l1 = intersect(l1, l2!!)
        return l1

    }

    private fun intersect(a: List<Integer>, b: List<Integer>): List<Integer> {
        val it1 = a.iterator()
        val it2 = b.iterator()

        val newList = ArrayList<Integer>()

        var l1: Integer? = popNextOrNull<Object>(it1)
        var l2: Integer? = popNextOrNull<Object>(it2)

        while (l1 != null && l2 != null) {
            if (l1 === l2) {
                newList.add(l1)
                l1 = popNextOrNull<Object>(it1)
                l2 = popNextOrNull<Object>(it2)
            } else if (l1 < l2) {
                l1 = popNextOrNull<Object>(it1)

            } else {
                l2 = popNextOrNull<Object>(it2)

            }
        }
        return newList

    }

    internal fun outputQueryResult(res: List<Integer>?): String {
        /*
		 * TODO:
		 *
		 * Take the list of documents ID and prepare the search results, sorted by lexicon order.
		 *
		 * E.g.
		 * 	0/fine.txt
		 *	0/hello.txt
		 *	1/bye.txt
		 *	2/fine.txt
		 *	2/hello.txt
		 *
		 * If there no matched document, output:
		 *
		 * no results found
		 *
		 * */

        val str = StringBuilder("")
        if (res == null) {
            return "NO RESULT FILE"
        }
        for (docId in res) {
            str.append(docDict.get(docId)).append("\n")
        }

        return str.toString()
    }

    protected fun finalize() {
        try {
            if (indexFile != null) indexFile!!.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    companion object {
        private val runindex: Index? = null

        private fun <X> popNextOrNull(iter: Iterator<X>): X? {
            return if (iter.hasNext()) {
                iter.next()
            } else {
                null
            }
        }

        @Throws(IOException::class)
        fun main(args: Array<String>) {
            /* Parse command line */
            if (args.size != 2) {
                System.err.println("Usage: java Query [Basic|VB|Gamma] index_dir")
                return
            }

            /* Get index */
            var className: String? = null
            try {
                className = args[0]
            } catch (e: Exception) {
                System.err
                        .println("Index method must be \"Basic\", \"VB\", or \"Gamma\"")
                throw RuntimeException(e)
            }

            /* Get index directory */
            val input = args[1]

            val queryService = Query()
            queryService.runQueryService(className, input)

            /* Processing queries */
            val br = BufferedReader(InputStreamReader(System.`in`))

            /* For each query */
            var line: String? = null
            while ((line = br.readLine()) != null) {
                val hitDocs = queryService.retrieve(line)
                queryService.outputQueryResult(hitDocs)
            }

            br.close()
        }
    }
}

