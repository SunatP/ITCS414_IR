/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071
 *  Sunat Praphanwong 6088130
 *  Barameerak Koonmongkon 6088156
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BasicIndex implements BaseIndex 
{
    private static final int INT_BYTES = Integer.SIZE / Byte.SIZE;

    @Override
    public PostingList readPosting(FileChannel fc)
    {
        /*
	 * TODO: Your code here
	 *       Read and return the postings list from the given file.
	 */
	ByteBuffer bytebuff = ByteBuffer.allocateDirect(INT_BYTES*2); // Use allocateDirect give the best I/O performance and decrease memory
        try 
        {
            if (fc.read(bytebuff) == -1) return null;
        } catch (IOException ex) 
        {
            Logger.getLogger(BasicIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
        bytebuff.rewind();
	PostingList p = new PostingList(bytebuff.getInt());
	int listLength = bytebuff.getInt();
	//Read in list
	bytebuff = ByteBuffer.allocate(INT_BYTES*listLength);
       try 
        {
            if (fc.read(bytebuff) == -1) return null;
        } 
       catch (IOException ex) 
       {
            Logger.getLogger(BasicIndex.class.getName()).log(Level.SEVERE, null, ex);
       }
        bytebuff.rewind();
	for (int i=0; i < listLength; i++)
        {
            p.getList().add(bytebuff.getInt());
	}
//        System.out.println(p.getList());
	return p;
    }

    @Override
    public void writePosting(FileChannel fc, PostingList p)
    {
        /*
	 * TODO: Your code here
	 *       Write the given postings list to the given file.
	 */
        ByteBuffer bytebuff = ByteBuffer.allocateDirect(INT_BYTES*(p.getList().size()+2)); // use Postinglist to approximate buffer
	bytebuff.putInt(p.getTermId());
	bytebuff.putInt(p.getList().size());
        p.getList().forEach((docID) -> {bytebuff.putInt(docID);});
	bytebuff.flip();
        try 
        {
            fc.write(bytebuff);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(BasicIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071
 *  Sunat Praphanwong 6088130
 *  Barameerak Koonmongkon 6088156
 **/
