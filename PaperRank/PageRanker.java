/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071 Sec 1
 *  Sunat Praphanwong 6088130 Sec 1
 *  Barameerak Koonmongkon 6088156 Sec 1
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This class implements PageRank algorithm on simple graph structure.
 * Put your name(s), ID(s), and section here.
 *
 */
public class PageRanker {

        private HashMap<Integer, PageRank> Pmap = new HashMap<>();
        private HashMap<Integer, HashSet<Integer>> Datastore = new HashMap<>();
        private double pageRank;    
        private double perplexity;
        private int Count = 1;
        private final int limiter = 4;
        private static final double factor = 0.85;
	/**
	 * This class reads the direct graph stored in the file "inputLinkFilename" into memory.
	 * Each line in the input file should have the following format:
	 * <pid_1> <pid_2> <pid_3> .. <pid_n>
	 * 
	 * Where pid_1, pid_2, ..., pid_n are the page IDs of the page having links to page pid_1. 
	 * You can assume that a page ID is an integer.
     * @param inputLinkFilename
     * @throws java.io.FileNotFoundException
	 */
	public void loadData(String inputLinkFilename) throws FileNotFoundException, IOException
        {      
            Stream<String> data;
            data = Files.lines(Paths.get(inputLinkFilename));
            
            data.forEach(line ->{
            String[] splits = line.split("\\s");
            int PID  = Integer.parseInt(splits[0]);
            if(!Pmap.containsKey(PID))
            {
                Pmap.put(PID, new PageRank(PID));
            }
            HashSet<Integer> Node = new HashSet<>();
            for(int i = 1 ; i < splits.length; i++)
            {
                final int PageID = Integer.parseInt(splits[i]);
                
                Node.add(PageID);
                
                if(!Pmap.containsKey(PageID))
                {
                    Pmap.put(PageID, new PageRank(PageID));
                }
                Pmap.get(PageID).incrementOutLinkCount();
            }
            Datastore.put(PID, Node);
//            System.out.println(Datastore);
            });
        }
	
	/**
	 * This method will be called after the graph is loaded into the memory.
	 * This method initialize the parameters for the PageRank algorithm including
	 * setting an initial weight to each page.
	 */
	public void initialize()
        {
            Pmap.entrySet().forEach((PWeight) -> {
                PWeight.getValue().setPageRank(1 / (double) Pmap.size());      
            });
           
//         if(false)System.out.println("Total Page : " + Pmap.size() );
        }
        
	/**
	 * Computes the perplexity of the current state of the graph. The definition
	 * of perplexity is given in the project specs.
     * @return 
	 */
	public double getPerplexity()
        {
            double entropy = 0;
//            if(false) System.out.println("Error"); // check integrity
//            for(Map.Entry<Integer,PageRank> Pentry : Pmap.entrySet())
//            {
//                PageRank pRank = Pentry.getValue();
//                entropy += pRank.getRank() * Math.log(pRank.getRank()) / Math.log(2);
//            }
            entropy = Pmap.entrySet().stream().map((Pweight) -> Pweight.getValue()).map((PR) -> PR.getRank() * Math.log(PR.getRank()) / Math.log(2)).reduce(entropy, (accumulator, _item) -> accumulator + _item);
//             Double mapsize = 1/(double)Pmap.size();
//            perplexity = Math.pow(2, entropy * -1);
//            System.out.println(entropy);
            return Math.pow(2, entropy * -1);
        }
	/**
	 * Returns true if the perplexity converges (hence, terminate the PageRank algorithm).
	 * Returns false otherwise (and PageRank algorithm continue to update the page scores). 
     * @return 
	 */
	public boolean isConverge()
        {
            double Newperplexity = getPerplexity();
            if(Math.floor(Newperplexity) % 10 == Math.floor(perplexity) % 10)
            {
                Count++;
            }
            else
            {
                Count = 1;
            }
            this.perplexity = Newperplexity;
            if(Count == limiter)
            {
                Count = 1;
                return true;
            }
            
            return false;      
        }
	
	/**
	 * The main method of PageRank algorithm. 
	 * Can assume that initialize() has been called before this method is invoked.
	 * While the algorithm is being run, this method should keep track of the perplexity
	 * after each iteration. 
	 * 
	 * Once the algorithm terminates, the method generates two output files.
	 * [1]	"perplexityOutFilename" lists the perplexity after each iteration on each line. 
	 * 		The output should look something like:
	 *  	
	 *  	183811
	 *  	79669.9
	 *  	86267.7
	 *  	72260.4
	 *  	75132.4
	 *  
	 *  Where, for example,the 183811 is the perplexity after the first iteration.
	 *
	 * [2] "prOutFilename" prints out the score for each page after the algorithm terminate.
	 * 		The output should look something like:
	 * 		
	 * 		1	0.1235
	 * 		2	0.3542
	 * 		3 	0.236
	 * 		
	 * Where, for example, 0.1235 is the PageRank score of page 1.
	 * 
     * @param perplexityOutFilename
     * @param prOutFilename
	 */
	public void runPageRank(String perplexityOutFilename, String prOutFilename) throws IOException
        {
            int CountPage = Pmap.size();
            List<String> perplexity = new ArrayList<>();
            List<String> Pagescore = new ArrayList<>();
            while(!isConverge())
            {
              double PageRank = 0;
              HashMap<Integer,Double> PRanks = new HashMap<>();
//              for(int PID: Pmap.keySet())
//              {
//                  if(Pmap.get(PID).isSinkPage())
//                  {
//                      PageRank += Pmap.get(PID).getRank();
//                  }
//              }
              PageRank = Pmap.keySet().stream().filter((PID) -> (Pmap.get(PID).isSinkPage())).map((PID) -> Pmap.get(PID).getRank()).reduce(PageRank, (accumulator, _item) -> accumulator + _item);
              for(int PID : Pmap.keySet())
              {
                  double newPRank = (1 - factor) / (double) CountPage;
                  newPRank += factor * PageRank / (double) CountPage;
                  if(Datastore.get(PID)!= null && !Datastore.get(PID).isEmpty())
                  {
//                      for(int page : Datastore.get(PID))
//                      {
//                          final PageRank newpage = Pmap.get(page);
//                          newPRank += factor  * newpage.getRank() / (double) newpage.getOutLinkCount();
//                      }
                      newPRank = Datastore.get(PID).stream().map((page) -> Pmap.get(page)).map((newpage) -> factor  * newpage.getRank() / (double) newpage.getOutLinkCount()).reduce(newPRank, (accumulator, _item) -> accumulator + _item);
                  }
                  PRanks.put(PID, newPRank);
              }
              
//              for(Map.Entry<Integer,PageRank> Page : Pmap.entrySet())
//              {
//                  Page.getValue().setPageRank(PRanks.get(Page.getKey()));
//              }
              Pmap.entrySet().forEach((Page) -> {
                  Page.getValue().setPageRank(PRanks.get(Page.getKey()));
                });
              perplexity.add(String.valueOf(getPerplexity()));
            }
//            for(Map.Entry<Integer,PageRank> Page : Pmap.entrySet())
//              {
//                  Pagescore.add(Page.getKey()+ " " + Page.getValue().getRank());
//              }
            Pmap.entrySet().forEach((Page) -> {
                Pagescore.add(Page.getKey()+ " " + Page.getValue().getRank());
            });
            WriteFileOut(perplexityOutFilename, perplexity);
            WriteFileOut(prOutFilename, Pagescore);
           
        }
        private void WriteFileOut(String file, List<String> line) throws IOException
        {
            Path pathout = Paths.get(file);
            File newfile = new File(file);
            
            if(newfile.exists())
            {
                newfile.delete();
            }
            Files.write(pathout, line, StandardCharsets.UTF_8);
        }
	
	
	/**
	 * Return the top K page IDs, whose scores are highest.
     * @param K
     * @return 
	 */
	public Integer[] getRankedPages(int K)
        {
            ArrayList<PageRank> PArray = new ArrayList<>(Pmap.values());
            PArray.sort(PageRank::compareTo);
            int arrsize = Math.min(K, PArray.size());
            Integer[] topResults = new Integer[arrsize];
            List<PageRank> sortResult = PArray.subList(0, arrsize);
            for(int i = 0; i < arrsize;i++)
            {
                topResults[i] = sortResult.get(i).getId();
            }
            return topResults;
        }
	
	public static void main(String args[]) throws IOException
	{
	long startTime = System.currentTimeMillis();
		PageRanker pageRanker =  new PageRanker();
		pageRanker.loadData("citeseer.dat");
		pageRanker.initialize();
		pageRanker.runPageRank("perplexity.out", "pr_scores.out");
		Integer[] rankedPages = pageRanker.getRankedPages(100);
	double estimatedTime = (double)(System.currentTimeMillis() - startTime)/1000.0;
		
		System.out.println("Top 100 Pages are:\n"+Arrays.toString(rankedPages));
		System.out.println("Proccessing time: "+estimatedTime+" seconds");
	}

    private static class PageRank implements Comparable{
        private int PageID,count = 0;
        private double PageRank;
              
        void setPageRank(double pageRank) 
        {
            this.PageRank = pageRank;
        }
        boolean isSinkPage() 
        {
            return count == 0;
        }
        int getId() 
        {
            return PageID;
        }
        double getRank()
        {
            return PageRank;
        }

        int getOutLinkCount() 
        {
            return count;
        }

        PageRank(int id) {
            this(id,0.0);
        }
        PageRank(int id , double PRank)
        {
            this.PageID = id;
            this.PageRank = PRank;
            
        }
        void incrementOutLinkCount() 
        {
            count++;
        }

        @Override
        public int compareTo(Object o) 
        {
        if(!(o instanceof PageRank))
        {
            return -1 ;
        }
        int Compare = Double.compare(((PageRank) o).PageRank,this.PageRank);
        if(Compare == 0)
        {
            return this.PageID - ((PageRank) o).PageID;
        }
            return Compare;
        }
    }
        
        
}
/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071 Sec 1
 *  Sunat Praphanwong 6088130 Sec 1
 *  Barameerak Koonmongkon 6088156 Sec 1
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/