# ITCS414 Information Storage and Retrieval

|Evaluation Method  | Weight  |
|:---|---:|
| Project 1  | 35% (group of 3) |
| Midterm Exam| 30% |
| Final Exam| 35% |
| Total | 100% |