/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071 Sec 1
 *  Sunat Praphanwong 6088130 Sec 1
 *  Barameerak Koonmongkon 6088156 Sec 1
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MyCoolSearcher extends Searcher {
    
    HashMap<String, Integer> DocFreqMap = new HashMap<>();
    HashMap<String, Double> Counter = new HashMap<>();
    
    public MyCoolSearcher(String docFilename) {
        super(docFilename);
        Set<String> TokenSet;
        int Docsize = documents.size();
        
        for(Document doc : documents)
        {
            TokenSet = new HashSet<>(doc.getTokens());
            for(String word : TokenSet)
            {
                if(!DocFreqMap.containsKey(doc))
                {
                    DocFreqMap.put(word, 1);
                }
                else
                {
                    DocFreqMap.replace(word, DocFreqMap.get(word+1));
                }
            }
        }
        DocFreqMap.keySet().forEach((word) -> {
            int DocFreq =  DocFreqMap.get(word);
            Counter.put(word, Math.log10((Docsize-DocFreq+0.5)/(DocFreq+0.5)));
        });
        
    }

    @Override
    public List<SearchResult> search(String queryString, int k) {
        List<String> StringArray = Searcher.tokenize(queryString);
        List<SearchResult> Result = new LinkedList<>();
        SearchResult searchresult;
        for(Document doc : documents)
        {
            double score = 0;
            Set<String> DocToken = new HashSet<>(doc.getTokens());
            Set<String> TokenSet = new HashSet<>(StringArray);
            TokenSet.retainAll(DocToken); // pick only element that Intersect
            // Use Functional Option
            score = TokenSet.stream().map((token) -> Counter.get(token)).reduce(score, (accumulator, _item) -> accumulator + _item); 
            // Replace from
            /**
            for(String token: TokenSet)
            {
                score += Counter.get(token);
            }
             **/
            searchresult = new SearchResult(doc,score);
            Result.add(searchresult);
            
        }
        Collections.sort(Result);
        return Result.subList(0, k);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    }
    
}
