/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071 Sec 1
 *  Sunat Praphanwong 6088130 Sec 1
 *  Barameerak Koonmongkon 6088156 Sec 1
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class JaccardSearcher extends Searcher{

	public JaccardSearcher(String docFilename) {
		
		/************* YOUR CODE HERE ******************/
		super(docFilename);
		/***********************************************/
	}

	@Override
	public List<SearchResult> search(String queryString, int k) {
		/************* YOUR CODE HERE ******************/
                List<String> Query = Searcher.tokenize(queryString);
                List<String> Term;
                List<SearchResult> result = new LinkedList<>();
                double score = 0;
                int unionsize,intersectsize;
                
                for(Document doc: documents)
                {
                    Set<String> Union = new HashSet<>(Query);
                    Set<String> Intersect = new HashSet<>(Query);
                    
                    Term = doc.getTokens();
                    Union.addAll(Term); // Use addAll or we call Union to add all element into HashSet
                    Intersect.retainAll(Term); // Use RetainAll to select and pick the element that has same in between 2 HashSet
                    unionsize = Union.size();
                    intersectsize = Intersect.size();
                    
                    if(unionsize == 0 || intersectsize == 0)
                        score = 0;
                    else
                        score = intersectsize/(double)unionsize;
                    result.add(new SearchResult(doc, score));
                }
                Collections.sort(result);
                return result.subList(0, k);
		/***********************************************/
	}

}
