/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071 Sec 1
 *  Sunat Praphanwong 6088130 Sec 1
 *  Barameerak Koonmongkon 6088156 Sec 1
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

public class SearcherEvaluator {
	private List<Document> queries = null;				//List of test queries. Each query can be treated as a Document object.
	private  Map<Integer, Set<Integer>> answers = null;	//Mapping between query ID and a set of relevant document IDs
	
	public List<Document> getQueries() {
		return queries;
	}

	public Map<Integer, Set<Integer>> getAnswers() {
		return answers;
	}

	/**
	 * Load queries into "queries"
	 * Load corresponding documents into "answers"
	 * Other initialization, depending on your design.
	 * @param corpus
	 */
	public SearcherEvaluator(String corpus)
	{
		String queryFilename = corpus+"/queries.txt";
		String answerFilename = corpus+"/relevance.txt";
		
		//load queries. Treat each query as a document. 
		this.queries = Searcher.parseDocumentFromFile(queryFilename);
		this.answers = new HashMap<>();
		//load answers
		try { // Optimized code with Try-Catch
			List<String> lines = FileUtils.readLines(new File(answerFilename), "UTF-8");
                        lines.stream().map((line) -> line.trim()).filter((line) -> !(line.isEmpty())).map((line) -> line.split("\\t")).forEachOrdered((parts) -> {
                            Integer qid = Integer.parseInt(parts[0]);
                            String[] docIDs = parts[1].trim().split("\\s+");
                            Set<Integer> relDocIDs = new HashSet<>();
                            for(String docID: docIDs)
                            {
                                relDocIDs.add(Integer.parseInt(docID));
                            }
                            this.answers.put(qid, relDocIDs);
                    });
		} catch (IOException e) {
                    // Doesn't need Execption 
		}
		
	}
	
	/**
	 * Returns an array of 3 numbers: precision, recall, F1, computed from the top *k* search results 
	 * returned from *searcher* for *query*
	 * @param query
	 * @param searcher
	 * @param k
	 * @return
	 */
	public double[] getQueryPRF(Document query, Searcher searcher, int k)
	{
            double prf[] = new double[3]; // Create array for calculate Precision Recall F1 score
            List<SearchResult> searchresult = searcher.search(query.getRawText(), k);
            Set<Integer> set1 = answers.get(query.getId()); 
            Set<Integer> retrieve = new HashSet<>();
            searchresult.forEach((searchR) -> {
                retrieve.add(searchR.getDocument().getId()); // Add document ID into Hashset
            });
            
            Set<Integer> getsamevalue = new HashSet<>(retrieve);
            getsamevalue.retainAll(set1); // Use retainAll for Intersect value
            
            if(retrieve.size() > 0)prf[0] = getsamevalue.size()/(double)retrieve.size(); // Calculate Precision
            if(set1.size()>0)prf[1] = getsamevalue.size()/(double)set1.size(); // Calculate Recall
            if(prf[0] > 0 && prf[1] > 0) 
            {
                prf[2] = (2*(prf[0]*prf[1])) / (prf[0]+prf[1]); // Calculate F1
            }
            
            
            
            
		/*********************** YOUR CODE HERE *************************/
            return prf;
		/****************************************************************/
	}
	
	/**
	 * Test all the queries in *queries*, from the top *k* search results returned by *searcher*
	 * and take the average of the precision, recall, and F1. 
	 * @param searcher
	 * @param k
	 * @return
	 */
	public double[] getAveragePRF(Searcher searcher, int k)
	{
            double average[] = new double[3];
            double temp[];
            int qsize = queries.size();
            
                for(Document doc : queries)
                {
                    temp = getQueryPRF(doc, searcher, k);
                    average[0] += temp[0];
                    average[1] += temp[1];
                    average[2] += temp[2];
                }
                average[0]/=(double)qsize;
                average[1]/=(double)qsize;
                average[2]/=(double)qsize;
		/*********************** YOUR CODE HERE *************************/
		return average;
		/****************************************************************/
	}
}
