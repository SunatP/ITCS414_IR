# PJ2 MuGle: One Step Closer to Google

## Due Date: Monday, 11 November 2019, 11:55 PM

### เราต้องโหลดอะไรบ้าง 
โค้ดของอาจารย์ เมื่อโหลดเสร็จแตกไฟล์ออกมาแล้วเริ่มเขียนโค้ดได้เลย

### ไฟล์อะไรบ้างที่ต้องแก้/เพิ่มโค้ด
- JaccardSeacher.java
- MyCoolSearcher.java (Bonus)
- SearcherEvaluator.java
- StudentTester.java (Main File แก้เฉพาะตอนรันโค้ด/เพิ่มโค้ดโบนัส)
- TFIDFSearcher.java

### เริ่มที่ JaccardSearcher.java

```java
public class JaccardSearcher extends Searcher{

	public JaccardSearcher(String docFilename) {
		super(docFilename);
		/************* YOUR CODE HERE ******************/
		
		/***********************************************/
	}

	@Override
	public List<SearchResult> search(String queryString, int k) {
		/************* YOUR CODE HERE ******************/
		return null;
		/***********************************************/
	}

}
```

ไฟล์ที่ให้อาจารย์ให้มานั้นมีฟังก์ชั่นที่กำหนดมาให้แล้วเราจะต้องทำการเพิ่มโค้ดให้สามารถรันได้อย่างสมบูรณ์

```java
	public JaccardSearcher(String docFilename) {
		super(docFilename);
	/************* YOUR CODE HERE ******************/
	// ตรงนี้ไม่ต้องเพิ่มอะไรเลย เพราะมีการใช้ super เพื่อโหลด document เข้ามาใช้งาน
	/***********************************************/
	}
```
เรามาสนใจฟังก์ชั่น **< List > search** กันดีกว่า
```java
@Override
public List<SearchResult> search(String queryString, int k) 
{
    List<String> Query = Searcher.tokenize(queryString); 
    // tokenize raw document and query text, to produce the same set of vocabulary
    List<String> Term;
    List<SearchResult> result = new LinkedList<>();
    double score = 0;
    int unionsize,intersectsize;
                
    for(Document doc: documents)
    {
        Set<String> Union = new HashSet<>(Query);
        Set<String> Intersect = new HashSet<>(Query);
                    
        Term = doc.getTokens();
        Union.addAll(Term); // Use addAll or we call Union to add all element into HashSet
        Intersect.retainAll(Term); // Use RetainAll to select and pick the element that has same in between 2 HashSet
        unionsize = Union.size();
        intersectsize = Intersect.size();
                    
        if(unionsize == 0 || intersectsize == 0)
            score = 0;
        else
            score = intersectsize/(double)unionsize;
            result.add(new SearchResult(doc, score));
    }
    Collections.sort(result);
    return result.subList(0, k);
}
```

### ต่อกันที่ SearcherEvaluator.java

```java
public class SearcherEvaluator {
	private List<Document> queries = null;				//List of test queries. Each query can be treated as a Document object.
	private  Map<Integer, Set<Integer>> answers = null;	//Mapping between query ID and a set of relevant document IDs
	
	public List<Document> getQueries() {
		return queries;
	}

	public Map<Integer, Set<Integer>> getAnswers() {
		return answers;
	}

	/**
	 * Load queries into "queries"
	 * Load corresponding documents into "answers"
	 * Other initialization, depending on your design.
	 * @param corpus
	 */
	public SearcherEvaluator(String corpus)
	{
		String queryFilename = corpus+"/queries.txt";
		String answerFilename = corpus+"/relevance.txt";
		
		//load queries. Treat each query as a document. 
		this.queries = Searcher.parseDocumentFromFile(queryFilename);
		this.answers = new HashMap<Integer, Set<Integer>>();
		//load answers
		try {
			List<String> lines = FileUtils.readLines(new File(answerFilename), "UTF-8");
			for(String line: lines)
			{
				line = line.trim();
				if(line.isEmpty()) continue;
				String[] parts = line.split("\\t");
				Integer qid = Integer.parseInt(parts[0]);
				String[] docIDs = parts[1].trim().split("\\s+");
				Set<Integer> relDocIDs = new HashSet<Integer>();
				for(String docID: docIDs)
				{
					relDocIDs.add(Integer.parseInt(docID));
				}
				this.answers.put(qid, relDocIDs);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Returns an array of 3 numbers: precision, recall, F1, computed from the top *k* search results 
	 * returned from *searcher* for *query*
	 * @param query
	 * @param searcher
	 * @param k
	 * @return
	 */
	public double[] getQueryPRF(Document query, Searcher searcher, int k)
	{
		/*********************** YOUR CODE HERE *************************/
		return null;
		/****************************************************************/
	}
	
	/**
	 * Test all the queries in *queries*, from the top *k* search results returned by *searcher*
	 * and take the average of the precision, recall, and F1. 
	 * @param searcher
	 * @param k
	 * @return
	 */
	public double[] getAveragePRF(Searcher searcher, int k)
	{
		/*********************** YOUR CODE HERE *************************/
		return null;
		/****************************************************************/
	}
}
```
เริ่มที่ฟังก์ชั่น **GetQueryPRF** เราจะสร้างอาเรย์ขึ้นมาเพื่อเอาไว้คิดค่า **PRF** หรือ **Precision** **Recall** และ **F1** 

```java
public double[] getQueryPRF(Document query, Searcher searcher, int k)
{
    double PRF_value[] = new double[3];
    List<SearchResult> result = searcher.search
    Set<Integer> set1 = answers.get(query.getId()); 
    Set<Integer> retrieve = new HashSet<>();        
    for(SearchResult results : result)
    {
        retrieve.add(results.getDocument().getId());
    }
    Set<Integer> getsamevalue = new HashSet<>(retrieve);
    getsamevalue.retainAll(set1); // Use retainAll for Intersect value    
    if(retrieve.size() > 0)PRF_value[0] = getsamevalue.size()/(double)retrieve.size(); // Calculate Precision
    if(set1.size()>0)PRF_value[1] = getsamevalue.size()/(double)set1.size(); // Calculate Recall
    if(PRF_value[0] > 0 && PRF_value[1] > 0) 
    {
        PRF_value[2] = (2*(PRF_value[0]*PRF_value[1])) / (PRF_value[0]+PRF_value[1]); // Calculate F1
    }
    return PRF_value;		
}
```

เมื่อเสร็จจากฟังก์ชั่น GetQueryPRF แล้วเราจะเริ่มเติมโค้ดลงไปใน **getAveragePRF** ต่อ

```java
public double[] getAveragePRF(Searcher searcher, int k)
{
    double averagePRF[] = new double[3];
    double temp[];
    int qsize = queries.size();
            
    for(Document doc : queries)
    {
        temp = getQueryPRF(doc, searcher, k);
        averagePRF[0] += temp[0];
        averagePRF[1] += temp[1];
        averagePRF[2] += temp[2];
    }
    averagePRF[0]/=(double)qsize;
    averagePRF[1]/=(double)qsize;
    aveaveragePRFrage[2]/=(double)qsize;

    return average;
}
```

### ต่อกันที่ TFIDFSearcher.java

```java
public class TFIDFSearcher extends Searcher
{	
	public TFIDFSearcher(String docFilename) {
		super(docFilename);
		/************* YOUR CODE HERE ******************/
		
		/***********************************************/
	}
	
	@Override
	public List<SearchResult> search(String queryString, int k) {
		/************* YOUR CODE HERE ******************/
		return null;
		/***********************************************/
	}
}
```

เราจะต้องสร้าง Hashmap ขึ้นมาเพื่อเก็บค่า IDF, Wordbag, Docmap และ Vector Cosinesimilarity และต้องสร้าง comparator ขึ้นมาเพื่อใช้ compare ค่าทั้งสองค่าที่ใน search
```java
public class TFIDFSearcher extends Searcher
{
    HashMap<String,Double> IDFMap = new HashMap<>();
    HashMap<Document,Integer> Docmap = new HashMap<>();
    HashMap<String, Integer> Wordbag = new HashMap<>(); // Create for union value between all token in all document    
    HashMap<Integer, double[]> Vector = new HashMap<>();
    Comparator<SearchResult> DocIDcompare = (SearchResult o1, SearchResult o2) -> o1.getDocument().getId()-o2.getDocument().getId(); // Compare Search 1 and Search 2 by using Comparator
    
    public TFIDFSearcher(String docFilename) 
    {
        double TFIDF, TF ; // Create for using in TFIDF and IF
        int termID = 0 ;
        int DocID = 0 ;
        double size = super.documents.size();
        List<String> token;
        Set<String> tokenSet;
        
        for (Document doc : super.documents) 
        {
            /** Create TermDict and DocDict**/
        if(!Docmap.containsKey(doc))
        {
            Docmap.put(doc, DocID);
            DocID++;
        }
        token = doc.getTokens();
        for (String string : token) 
            {
                if(!Wordbag.containsKey(string))
                    {
                        Wordbag.put(string, termID);
                        termID++;
                    }
            }
        }
        
        for (Document doc : super.documents) 
        {
            /** Find IDF by using For Loop **/
        tokenSet = new HashSet<>(doc.getTokens());
        for(String string: tokenSet)
            {
            if(!InvertDocFreqMap.containsKey(string))
            {
                InvertDocFreqMap.put(string, 1.0);
            }
            else InvertDocFreqMap.replace(string, InvertDocFreqMap.get(string)+1.0);
            }
        }
    // InvertDocFreqMap.keySet().forEach((string) -> { 
    // InvertDocFreqMap.replace(string, Math.log10(1.0 + (size/InvertDocFreqMap.get(string)))); // Calculate with LogNormalization Base 10
    for(String string: IDFMap.keySet())
    {
        IDFMap.replace(string, Math.log10(1.0 + (size/InvertDocFreqMap.get(string))));
    }
    // });
        
    for (Document doc : documents)
    {
        double vector[] = new double[Wordbag.size()];
        token = doc.getTokens();
        for (String word : token) 
        {
            TF = Collections.frequency(token, word);
            if(TF > 0) TF = 1 + Math.log10(TF);
            TFIDF = InvertDocFreqMap.get(word) * TF; // TFIDF from IDF(Get value from word in token) * TF 
            vector[Wordbag.get(word)] = TFIDF;
        }
        Vector.put(Docmap.get(doc), vector);
        }
    }

    @Override
    public List<SearchResult> search(String queryString, int k) 
{
    List<String> StrArray = Searcher.tokenize(queryString);
    List<SearchResult> results = new LinkedList<>();
    List<SearchResult> NaNresults = new LinkedList<>();
    PriorityQueue<SearchResult> resultqueue = new PriorityQueue<>(DocIDcompare);
    SearchResult cosine;
    double TF = 0,score = 0.0;
    double normd , normq , qd , Freq;
    double q[], d[]; // Create for calculate Vector
    int docid , i ;
                
    // Use for loop to Calculate Query Vector 
    for(Document doc: Docmap.keySet())
    {
        normd = 0 ; normq = 0; qd = 0;
        docid = Docmap.get(doc);
        d = Vector.get(docid);
        q = new double[Wordbag.size()];
                    
        for(String string: StrArray)
        {
            if(Wordbag.get(string) != null)
            {
                Freq = Collections.frequency(StrArray, string); // Get the frequency of element from tokenize query as number
                TF = 1 + Math.log10(Freq);
                q[Wordbag.get(string)] = TF*InvertDocFreqMap.get(string); // TF-IDF = TF * IDF
            }
        }
        for(i = 0 ; i < d.length ; i++)
        {
            qd += d[i] * q[i];
            normd += Math.pow(d[i], 2);
            normq += Math.pow(q[i], 2);
        }
        normd = Math.sqrt(normd);
        normq = Math.sqrt(normq);
        score = qd/(normd*normq); // Find score of Cosine
        cosine = new SearchResult(doc, score); // Get the result of Cosine Similarity
        if(Double.isNaN(score)) // if Score is not a number(NaN)
        {
            resultqueue.add(cosine);
        }
        else
            results.add(cosine);
        }
        if(results.size()> 0) // Least result should be k items
        {
            Collections.sort(results);  
            return results.subList(0, k);
        }
        else // If we found Not a number result
        {
            for(i = 0 ; i < k ; i++)
            NaNresults.add(resultqueue.poll()); // retrieves and remove value out
            return NaNresults.subList(0, k);
            } 
	}
}
```